import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-test',   // three ways 1 app-test, 2 .app-test, 3 [app-test]
  template:   `<div> 
                <h1>Inline Template</h1>
                <h2>Hello, this is the inline text!</h2>
              </div>

              <button (click)="onClick()">Greet</button><br><br>
              <button (click)="greetuser='Welcome Teena'">Greet</button><br/>
              {{greetuser}}
              <br><br>
              <input #myInput type="text">
              <button (click)="logMessage(myInput.value)">Log</button><br/><br/>

              <input [(ngModel)]="name" type="text">
              {{name}}

              <h2 *ngIf = "true">
                Structural Binding Ng-If
              </h2>
              
              <div *ngIf = "displayName; then thenBlock; else elseBlock"></div>
              
              <ng-template #thenBlock>
                <h2>
                Veiw
                </h2>
              </ng-template>
              <ng-template #elseBlock>
                <h2>
                hidden
                </h2>
              </ng-template> 

              <div [ngSwitch]="color" id ="1">
                <div *ngSwitchCase = "'red'"> You Picked red color</div>
                <div *ngSwitchCase = "'blue'"> You Picked blue color</div>
                <div *ngSwitchCase = "'green'"> You Picked green color</div>
                <div *ngSwitchDefault> Pick Again</div>
              </div>

              <div *ngFor="let color of colors; index as i">
                <h2>{{i}} {{color}}</h2>
              </div> 

              <div *ngFor="let color of colors; last as l">
                <h2>{{l}} {{color}}</h2>
              </div> 
              
              <div *ngFor="let color of colors; odd as o">
                <h2>{{o}} {{color}}</h2>
              </div>

              <h2>{{"Welcome to " + parentData}}</h2>
              <button (click) = "fireEvent()">Send Event</button>
              `,
  styles: [ ]
})
export class TestComponent implements OnInit {

  public greetuser = "";
  public name="";
  public displayName= false;
  public color = "orange";

  public colors =["red","blue","green","yellow"];
  @Input() public parentData: any;
  //@Input('parentData') public interaction: any;
  @Output() public childEvent = new EventEmitter;

  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    console.log('Welcome to the page!');
    this.greetuser='Welcome to the page!'
  }
  logMessage(value: any){
    console.log(value);
  }
  fireEvent(){
    this.childEvent.emit("Hey, Welcome to Component Interaction");
  }

}