import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() loggedIn!: boolean;
  @Output() event = new EventEmitter
  constructor() { }

  ngOnInit(): void {
  }

  welcome(){
    //alert("Thank you for coming to this page!!")
    this.event.emit()
  }
}
