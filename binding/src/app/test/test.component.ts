import { style } from '@angular/animations';
import { getCurrencySymbol } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template: `
    <h2>
      Welcome to the page!
    </h2>
    <h2>
      Welcome {{name}} 
    </h2>
    <h2>
      {{2+2}} 
    </h2>
    <h2>
      {{"Welcome" + " " + name}} 
    </h2>
    <h2>
      {{name.length}}
    </h2>
    <h2>
      {{name.toUpperCase()}}
    </h2>
    <h2>
      {{greatuser()}}
    </h2>
    <h2>
      {{siteUrl}}
    </h2>

    <input [id]="myid" type="text" value="Property Binding">
    <input [disabled] = "true" id="{{myid}}" type="text" value="Property Binding">

    <h2 [class] = "successclass">
    Class Binding
    </h2>
    <h2 [class] = "dangerclass">
    Class Binding
    </h2>
    <h2 [class] = "specialclass">
    Class Binding
    </h2>
    <h2 [class.text-danger] = "hasError">
      Class Binding
    </h2>
    <h2 [ngClass]="message">
      Welcome to Class Binding!
    </h2>
    
    <h2 [style.color]="hasError ? 'blue' : 'orange'">
      Style Binding
    </h2>
    <h2 [style.color]="highlightcolor">
      Style Binding
    </h2>
    <h2 [ngStyle]="titlestyle">
      Style Binding
    </h2>
  `, // name come from the class in welcome {{name}}
  // we can use bind-disabled = "false"
  styles: [
    `
    h2{
      text-align: center;
      color: purple;
    }
    input{
      text-align: center;
    }
    .text-success{
      color: green;
    }
    .text-danger{
      color: red;
    }
    .text-special{
      font-style: italic;
    }`
  ]
})
export class TestComponent implements OnInit {

  public name = "Teena Thakur";
  public siteUrl = window.location.href;
  public myid = "testId";
  public successclass = "text-success";
  public dangerclass = "text-danger";
  public specialclass = "text-special";
  public hasError = false;
  public isSpecial = true;
  public message = {
    "text-success": !this.hasError,
    "text-danger": this.hasError,
    "text-special": this.isSpecial
  }
  public highlightcolor = "yellow";
  public titlestyle = {
    color: "blue",
    fontStyle: "italic"
  }

  constructor() { }

  ngOnInit(): void {
  }

  greatuser(){
    return "Hello" + " " + this.name;
  }

}
